import json
import os


def get_data(dirname: str):
    data = {}
    with os.scandir(dirname) as scan:
        for entry in scan:
            if entry.is_file():
                if entry.name.endswith('.json'):
                    with open(entry.path) as json_file:
                        data[entry.name[:-5]] = json.load(json_file)
            elif entry.is_dir():
                data[entry.name] = get_data(entry.path)
    return data
